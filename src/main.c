/*
 * sprpack/main.c
 *
 * Copyright (C) 2022 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Small utility to pack / unpack sprites into / from atlases
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "libpng/png.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_FAILURE_STRINGS
#include "stb_image.h"

#define STB_RECT_PACK_IMPLEMENTATION
#include "stb_rect_pack.h"

#ifdef __WIN32__
#define SEP '\\'
#else
#define SEP '/'
#endif

/* meta info formats */
enum { JSON, XML, TXT, SEXPR, CHDR, TNG };
const char *exts[] = { ".json", ".xml", ".txt", ".se", ".h", ".txt" };

/* command line arguments */
int width = 0, height = 0, unpack = 0, crop = 0, inpcrop = 1, fmt = JSON, tofile = 0;
char *comment = NULL;

/* internal variables */
typedef struct {
    char *name;
    int X, Y, W, H;
    uint8_t *data;
} meta_t;
char full[8192];
meta_t *files = NULL;
struct stbrp_rect *rects = NULL;
int num = 0, maxw = 0, maxh = 0;
int64_t prod = 0;

/**
 * Load an image
 */
unsigned char *image_load(char *fn, int *w, int *h)
{
    FILE *f;
    stbi__context s;
    stbi__result_info ri;
    unsigned char *data = NULL;
    int c = 0, nf = 1;

    *w = *h = 0;
    f = stbi__fopen(fn, "rb");
    if(!f) return NULL;
    stbi__start_file(&s, f);
    if(stbi__gif_test(&s)) {
        data = stbi__load_gif_main(&s, NULL, w, h, &nf, &c, 4);
        if(data && *w > 0 && *h > 0 && nf > 1)
            *h *= nf;
    } else {
        data = stbi__load_main(&s, w, h, &c, 4, &ri, 8);
    }
    fclose(f);
    if(data && *w > 0 && *h > 0)
        return data;
    if(data) free(data);
    return NULL;
}

/**
 * Write image to file
 */
int image_save(uint8_t *p, int w, int h, char *fn, char *meta)
{
    FILE *f;
    uint32_t *ptr = (uint32_t*)p, pal[256];
    uint8_t *data, *pal2 = (uint8_t*)&pal;
    png_color pngpal[256];
    png_byte pngtrn[256];
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep *rows;
    png_text texts[1] = { 0 };
    int i, j, nc = 0;

    if(!p || !fn || !*fn || w < 1 || h < 1) return 0;
    printf("Saving %s\r\n", fn);
    f = fopen(fn, "wb+");
    if(!f) { fprintf(stderr,"Unable to write %s\r\n", fn); exit(2); }
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) { fclose(f); return 0; }
    info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) { png_destroy_write_struct(&png_ptr, NULL); fclose(f); return 0; }
    if(setjmp(png_jmpbuf(png_ptr))) { png_destroy_write_struct(&png_ptr, &info_ptr); fclose(f); return 0; }
    png_init_io(png_ptr, f);
    png_set_compression_level(png_ptr, 9);
    png_set_compression_strategy(png_ptr, 0);
    png_set_filter(png_ptr, PNG_FILTER_TYPE_BASE, PNG_FILTER_VALUE_SUB);
    rows = (png_bytep*)malloc(h * sizeof(png_bytep));
    data = (uint8_t*)malloc(w * h);
    if(!rows || !data) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
    /* lets see if we can save this as an indexed image */
    for(i = 0; i < w * h; i++) {
        for(j = 0; j < nc && pal[j] != ptr[i]; j++);
        if(j >= nc) {
            if(nc == 256) { nc = -1; break; }
            pal[nc++] = ptr[i];
        }
        data[i] = j;
    }
    if(nc != -1) {
        for(i = j = 0; i < nc; i++) {
            pngpal[i].red = pal2[i * 4 + 0];
            pngpal[i].green = pal2[i * 4 + 1];
            pngpal[i].blue = pal2[i * 4 + 2];
            pngtrn[i] = pal2[i * 4 + 3];
        }
        png_set_PLTE(png_ptr, info_ptr, pngpal, nc);
        png_set_tRNS(png_ptr, info_ptr, pngtrn, nc, NULL);
        for(i = 0; i < h; i++) rows[i] = data + i * w;
    } else
        for(i = 0; i < h; i++) rows[i] = p + i * w * 4;
    png_set_IHDR(png_ptr, info_ptr, w, h, 8, nc == -1 ? PNG_COLOR_TYPE_RGB_ALPHA : PNG_COLOR_TYPE_PALETTE,
        PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    if(meta && *meta) {
        texts[0].key = "Comment"; texts[0].text = meta;
        png_set_text(png_ptr, info_ptr, texts, 1);
    }
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(rows);
    free(data);
    fclose(f);
    return 1;
}

/**
 * Recursively add image files to list
 */
void find(char *name, int lvl)
{
    DIR *dir;
    struct dirent *ent;
    int i, l, x, y, w, h;
    uint8_t *p;
    char *com, *s, *d, *e;

    if(!name || !*name || lvl > 8191 - 256) return;
    if((dir = opendir(name)) != NULL) {
        if(name != full) { strcpy(full, name); lvl = strlen(full); }
        if(lvl > 0 && full[lvl-1] != SEP) full[lvl++] = SEP;
        full[lvl] = 0;
        while ((ent = readdir(dir)) != NULL) {
            if(ent->d_name[0] == '.') continue;
            strcpy(full + lvl, ent->d_name);
            find(full, lvl + strlen(ent->d_name));
        }
        closedir(dir);
    } else {
        if((p = image_load(name, &w, &h)) && w > 0 && h > 0) {
            i = num++;
            files = (meta_t*)realloc(files, num * sizeof(meta_t));
            rects = (struct stbrp_rect*)realloc(rects, num * sizeof(struct stbrp_rect));
            if(!files || !rects) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            memset(&files[i], 0, sizeof(meta_t));
            l = strlen(name);
            files[i].name = (char*)malloc(l + 1);
            if(!files[i].name) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            e = strrchr(name, '.'); if(!e) e = name + l;
            for(s = name, d = files[i].name; s < e && *s; s++)
                *d++ = *s == '/' || *s == '\\' || *s == ':' || *s == '\"' || *s == ' ' || *s == '\t' ||
                    *s == '\n' || *s == '\r' ? '_' : *s;
            *d = 0;
            files[i].data = p;
            files[i].W = w; files[i].H = h;
            if(w > maxw) maxw = w;
            if(h > maxh) maxh = h;
            prod += (int64_t)w * (int64_t)h;
            /* crop input sprite to content */
            if(inpcrop) {
                for(y = 0; y < files[i].H && h > 0; y++) {
                    for(x = 0; x < files[i].W && !p[(y * files[i].W + x) * 4 + 3]; x++);
                    if(x < files[i].W) break;
                    files[i].Y++; h--;
                }
                for(y = files[i].H - 1; y >= files[i].Y && h > 0; y--) {
                    for(x = 0; x < files[i].W && !p[(y * files[i].W + x) * 4 + 3]; x++);
                    if(x < files[i].W) break;
                    h--;
                }
                for(x = 0; x < files[i].W && h > 0 && w > 0; x++) {
                    for(y = 0; y < h && !p[((y + files[i].Y) * files[i].W + x) * 4 + 3]; y++);
                    if(y < h) break;
                    files[i].X++; w--;
                }
                for(x = files[i].W - 1; x >= files[i].X && h > 0 && w > 0; x--) {
                    for(y = 0; y < h && !p[((y + files[i].Y) * files[i].W + x) * 4 + 3]; y++);
                    if(y < h) break;
                    w--;
                }
                if(w < 1 || h < 1) w = h = files[i].X = files[i].Y = 0;
            }
            memset(&rects[i], 0, sizeof(struct stbrp_rect));
            rects[i].id = i; rects[i].w = w; rects[i].h = h;
        } else
            if(p) free(p);
        com = stbi_comment(); if(com) free(com);
    }
}

/**
 * Usage instructions
 */
void usage()
{
    printf("sprpack by bzt Copyright (C) 2022 MIT license\r\n https://gitlab.com/bztsrc/spratlas\r\n\r\n");
    printf("./sprpack [-s <w>[,<h>]] [-c] [-j|-x|-t|-T|-e|-h] [-f] <output png> <inputs>\r\n");
    printf("./sprpack [-u|-l] <atlas png>\r\n\r\n");
    printf(" -s <w>,<h>      specify the output image's size (defaults to optimal)\r\n");
    printf(" -c              crop output image to contents\r\n");
    printf(" -d              do not crop input images\r\n");
    printf(" -j              save meta info in JSON format (default)\r\n");
    printf(" -x              save meta info in XML format\r\n");
    printf(" -t              save meta info in tab-text format\r\n");
    printf(" -T              save meta info in TirNanoG Atlas format\r\n");
    printf(" -e              save meta info in S-Expression format\r\n");
    printf(" -h              save meta info in C/C++ header format (implies -f)\r\n");
    printf(" -f              save meta info into separate file too\r\n");
    printf(" <output png>    output image, always (indexed or truecolor) png with alpha\r\n");
    printf(" <inputs>        input image(s) (png, jpg, gif, tga, bmp, etc.) or directories\r\n");
    printf(" -u <atlas png>  unpack an atlas\r\n");
    printf(" -l <atlas png>  list sprites in an atlas (same format as stored)\r\n\r\n");
    exit(0);
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
    stbrp_context ctx;
    stbrp_node *nodes;
    uint8_t *o, *p, *src, *dst;
    char *png = NULL, *meta = NULL, *s, *e, fn[8192], bs, be;
    int i, j, l = 0, x, y, w, h, X, Y, W, H;
    FILE *f;

    /* parse command line */
    for(i = 1; i < argc && !png; i++)
        if(argv[i][0] == '-') {
            if(argv[i][1] == 's') {
                if(sscanf(argv[++i], "%d,%d", &width, &height) == 1) height = width;
            } else
                for(j = 1; argv[i][j]; j++)
                    switch(argv[i][j]) {
                        case 'c': crop = 1; break;
                        case 'd': inpcrop = 0; break;
                        case 'u': unpack = 1; break;
                        case 'l': unpack = 2; break;
                        case 'j': fmt = JSON; break;
                        case 'x': fmt = XML; break;
                        case 't': fmt = TXT; break;
                        case 'e': fmt = SEXPR; break;
                        case 'h': fmt = CHDR; tofile = 1; break;
                        case 'T': fmt = TNG; break;
                        case 'f': tofile = 1; break;
                        default: fprintf(stderr,"Unknown flag '%c'\r\n", argv[i][1]); exit(2);
                    }
        } else {
            png = argv[i];
        }
    if(!png || (!unpack && i >= argc)) usage();

    if(!unpack) {
        if(fmt == TNG) { crop = 1; tofile = inpcrop = 0; }
        /* add sprites from source images to output */
        for(; i < argc; i++)
            find(argv[i], 0);
        /* calculate an optimal atlas size if not given */
        l = 0;
        if(!width || !height) {
            l = crop = 1;
            for(i = 0; i * i < prod; i++);
            if(i < maxw) i = maxw;
            for(width = 1; width < i; width <<= 1);
            i = (prod + width - 1) / width;
            if(i < maxh) i = maxh;
            for(height = 1; height < i; height <<= 1);
        }
        nodes = (stbrp_node*)malloc((width + 1) * sizeof(stbrp_node));
        if(!nodes) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
        memset(nodes, 0, (width + 1) * sizeof(stbrp_node));
        stbrp_init_target(&ctx, width, height, nodes, width + 1);
        if(!stbrp_pack_rects(&ctx, rects, num)) {
            if(l) {
                height <<= 1;
                memset(nodes, 0, (width + 1) * sizeof(stbrp_node));
                for(i = 0; i < num; i++) rects[i].was_packed = rects[i].x = rects[i].y = 0;
                stbrp_init_target(&ctx, width, height, nodes, width + 1);
                if(stbrp_pack_rects(&ctx, rects, num)) goto ok;
            }
            fprintf(stderr,"Error, sprites do not fit into %u x %u atlas.\r\n", width, height);
            exit(2);
        }
ok:     free(nodes);
        if(crop) width = height = 0;
        for(i = l = 0; i < num; i++) {
            if(crop) {
                if(rects[i].x + rects[i].w > width) width = rects[i].x + rects[i].w;
                if(rects[i].y + rects[i].h > height) height = rects[i].y + rects[i].h;
            }
            l += strlen(files[i].name) + 256;
        }
        if(width > 0 && height > 0) {
            meta = s = (char*)malloc(l);
            if(!meta) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            memset(meta, 0, l);
            o = (uint8_t*)malloc(width * height * 4);
            if(!o) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            memset(o, 0, width * height * 4);
            /* header */
            switch(fmt) {
                case TNG: s += sprintf(s, "TirNanoG Atlas\n"); break;
                case CHDR:
                    e = strrchr(png, '.'); if(!e) e = png + strlen(png);
                    for(i = 0; png + i < e; i++)
                        fn[i] = png[i] == '_' || (png[i] >= 'a' && png[i] <= 'z') || (png[i] >= 'A' && png[i] <= 'Z') ||
                            (i && png[i] >= '0' && png[i] <= '0') ? png[i] : '_';
                    s += sprintf(s, "#ifndef SPRPACKT\r\n"
                    "#define SPRPACKT\r\n"
                    "typedef struct { int x, y, w, h, X, Y, W, H; const char *name; } sprpack_t;\r\n#endif\r\n"
                    "sprpack_t %s[] = {\r\n", fn); break;
                case SEXPR: s += sprintf(s, "(sprites\r\n"); break;
                case TXT: break;
                case XML: s += sprintf(s, "<sprites>\r\n"); break;
                default: s += sprintf(s, "[\r\n"); break;
            }
            /* records */
            for(i = 0; i < num; i++) {
                if(rects[i].w > 0 && rects[i].h > 0) {
                    src = files[i].data + (files[i].Y * files[i].W + files[i].X) * 4;
                    dst = o + (width * rects[i].y + rects[i].x) * 4;
                    for(j = 0; j < rects[i].h; j++, dst += width * 4, src += files[i].W * 4)
                        memcpy(dst, src, rects[i].w * 4);
                }
                switch(fmt) {
                    case TNG:
                        s += sprintf(s, "%u %u %u %u %u %u %u %u 0 %s\n", rects[i].x, rects[i].y, rects[i].w, rects[i].h,
                            files[rects[i].id].X, files[rects[i].id].Y, files[rects[i].id].W, files[rects[i].id].H,
                            files[rects[i].id].name);
                    break;
                    case CHDR:
                        s += sprintf(s, "%s { %u, %u, %u, %u, %u, %u, %u, %u, \"%s\" }",
                            i ? ",\r\n" : "", rects[i].x, rects[i].y, rects[i].w, rects[i].h,
                            files[rects[i].id].X, files[rects[i].id].Y, files[rects[i].id].W, files[rects[i].id].H,
                            files[rects[i].id].name);
                    break;
                    case SEXPR:
                        s += sprintf(s, "(sprite %u %u %u %u %u %u %u %u \"%s\")\r\n",
                            rects[i].x, rects[i].y, rects[i].w, rects[i].h,
                            files[rects[i].id].X, files[rects[i].id].Y, files[rects[i].id].W, files[rects[i].id].H,
                            files[rects[i].id].name);
                    break;
                    case TXT:
                        s += sprintf(s, "%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%s\r\n",
                            rects[i].x, rects[i].y, rects[i].w, rects[i].h,
                            files[rects[i].id].X, files[rects[i].id].Y, files[rects[i].id].W, files[rects[i].id].H,
                            files[rects[i].id].name);
                    break;
                    case XML:
                        s += sprintf(s, "<sprite x=\"%u\" y=\"%u\" w=\"%u\" h=\"%u\" "
                            "X=\"%u\" Y=\"%u\" W=\"%u\" H=\"%u\" name=\"%s\" />\r\n",
                            rects[i].x, rects[i].y, rects[i].w, rects[i].h,
                            files[rects[i].id].X, files[rects[i].id].Y, files[rects[i].id].W, files[rects[i].id].H,
                            files[rects[i].id].name);
                    break;
                    default:
                        s += sprintf(s, "%s{ \"x\": %u, \"y\": %u, \"w\": %u, \"h\": %u, "
                            "\"X\": %u, \"Y\": %u, \"W\": %u, \"H\": %u, \"name\": \"%s\" }",
                            i ? ",\r\n" : "", rects[i].x, rects[i].y, rects[i].w, rects[i].h,
                            files[rects[i].id].X, files[rects[i].id].Y, files[rects[i].id].W, files[rects[i].id].H,
                            files[rects[i].id].name);
                    break;
                }
            }
            /* footer */
            switch(fmt) {
                case TNG: break;
                case CHDR: s += sprintf(s, "\r\n};"); break;
                case SEXPR: s += sprintf(s, ")"); break;
                case TXT: break;
                case XML: s += sprintf(s, "</sprites>"); break;
                default: s += sprintf(s, "\r\n]"); break;
            }
            image_save(o, width, height, png, meta);
            free(o);
            /* save meta info to a separate file too */
            if(tofile) {
                e = strrchr(png, '.'); if(!e) e = png + strlen(png);
                memcpy(fn, png, e - png);
                strcpy(fn + (e - png), exts[fmt]);
                printf("Saving %s\r\n", fn);
                f = fopen(fn, "wb+");
                if(f) {
                    fwrite(meta, 1, s - meta, f);
                    fclose(f);
                }
            }
            free(meta);
        } else
            fprintf(stderr,"Error, nothing left after cropping to contents?\r\n");
        for(i = 0; i < num; i++) {
            if(files[i].name) free(files[i].name);
            if(files[i].data) free(files[i].data);
        }
        free(files);
        free(rects);
    } else {
        /* extract sprites from atlas input */
        p = image_load(png, &width, &height);
        comment = s = stbi_comment();
        if(!p || !comment) { printf("\r\n"); fprintf(stderr,"Unable to load %s\r\n", png); exit(1); }
        if(unpack == 2) {
            /* just list sprites meta info */
            printf("%s\r\n", comment);
        } else {
            /* detect format */
            for(s = comment; *s && (*s == ' ' || *s == '\r' || *s == '\n'); s++);
            if(*s == 'T') { fmt = TNG; bs = '0'; be = '9'; } else
            if(*s == '(') { fmt = SEXPR; bs = be = '('; s++; } else
            if(*s == '[') { fmt = JSON; bs = be = '{'; } else
            if(*s == '<') { fmt = XML; bs = be = '<'; } else
            if(*s >= '0' && *s <= '9') { fmt = TXT; bs = '0'; be = '9'; } else
                { fmt = CHDR; bs = be = '{'; while(*s && *s != '=') s++; }
            while(*s) {
                while(*s && (*s < bs || *s > be)) s++;
                if(*s) {
                    x = y = w = h = X = Y = W = H = 0; fn[0] = 0;
                    while(*s && *s != '\n' && *s != '\r') {
                        switch(fmt) {
                            case CHDR:
                            case SEXPR: /* parse C/C++ header or S-Expression */
                                if(*s == bs) {
                                    while(*s && (*s < '0' || *s > '9')) s++;
                                    x = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    y = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    w = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    h = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    X = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    Y = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    W = atoi(s); while(*s && *s != ' ' && *s != ',') { s++; } while(*s == ' ' || *s == ',') s++;
                                    H = atoi(s); while(*s && *s != '\"') { s++; } s++;
                                    for(e = s; *e && *e != '\"' && *e != '\r' && *e != '\n'; e++);
                                    memcpy(fn, s, e - s); fn[e - s] = 0;
                                    s = e - 1;
                                }
                            break;
                            case TNG: /* parse TirNanoG */
                            case TXT: /* parse tab-text */
                                x = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                y = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                w = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                h = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                X = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                Y = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                W = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                H = atoi(s); while(*s && *s != '\t' && *s != ' ') { s++; } s++;
                                if(fmt == TNG) { while(*s && *s != '\t' && *s != ' ') { s++; } s++; }
                                for(e = s; *e && *e != '\n' && *e != '\r'; e++);
                                memcpy(fn, s, e - s); fn[e - s] = 0;
                                s = e - 1;
                            break;
                            case XML: /* parse XML */
                                if(*s == '=') {
                                    for(e = s; *e && *e != '\"'; e++);
                                    if(*e) e++;
                                    switch(s[-1]) {
                                        case 'x': x = atoi(e); break;
                                        case 'y': y = atoi(e); break;
                                        case 'w': w = atoi(e); break;
                                        case 'h': h = atoi(e); break;
                                        case 'X': X = atoi(e); break;
                                        case 'Y': Y = atoi(e); break;
                                        case 'W': W = atoi(e); break;
                                        case 'H': H = atoi(e); break;
                                        case 'e':
                                            for(s = e; *e && *e != '\"' && *e != '>' && *e != '\r' && *e != '\n'; e++);
                                            memcpy(fn, s, e - s); fn[e - s] = 0;
                                            s = e - 1;
                                        break;
                                    }
                                }
                            break;
                            default: /* parse JSON */
                                if(*s == '\"') {
                                    s++; for(e = s; *e && *e != ':'; e++);
                                    for(; *e && (*e == ':' || *e == ' ' || *e == '\"'); e++);
                                    switch(*s) {
                                        case 'x': x = atoi(e); break;
                                        case 'y': y = atoi(e); break;
                                        case 'w': w = atoi(e); break;
                                        case 'h': h = atoi(e); break;
                                        case 'X': X = atoi(e); break;
                                        case 'Y': Y = atoi(e); break;
                                        case 'W': W = atoi(e); break;
                                        case 'H': H = atoi(e); break;
                                        case 'n':
                                            for(s = e; *e && *e != '\"' && *e != '}' && *e != '\r' && *e != '\n'; e++);
                                            memcpy(fn, s, e - s); fn[e - s] = 0;
                                            e--;
                                        break;
                                    }
                                    for(s = e; *s && *s != ',' && *s != '}'; s++);
                                }
                            break;
                        }
                        s++;
                    }
                    /* if we got all the fields, and their values are valid, save png */
                    if(x >= 0 && y >= 0 && X >= 0 && Y >= 0 && W > 0 && H > 0 && fn[0] &&
                      x + w <= width && y + h <= height && X + w <= W && Y + h <= H) {
                        strcat(fn, ".png");
                        o = (uint8_t*)malloc(W * H * 4);
                        if(!o) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
                        memset(o, 0, W * H * 4);
                        if(w > 0 && h > 0) {
                            src = p + (width * y + x) * 4; dst = o + (W * Y + X) * 4;
                            for(j = 0; j < h; j++, dst += W * 4, src += width * 4)
                                memcpy(dst, src, w * 4);
                        }
                        image_save(o, W, H, fn, NULL);
                        free(o);
                    }
                }
            }
        }
        if(comment) free(comment);
        if(p) free(p);
    }

    return 0;
}
